var x = 50; //x-koordinatet
var y = 50; //y-koordinatet
var b = 30; //b for bredten
var h = 30; //h for højden

function setup() {
  
  createCanvas(500, 500); //lav et canvas på 500x500 pixels
  background(255, 220, 220); //gør baggrunden for canvas lyderød
  frameRate(5); //sætter et lavere framerate, så man ikke får et epileptisk anfald :P

}

function draw() {
  
  noStroke(); //bestemmer at der ikke skal være nogen kant
  fill(random(100, 255), random(100, 255), random(100, 255)); //bestemmer at farven på formen skal have random RGB værdier

  b = random(10, 350); //variablen for bredte er random

  rect(x, y, b, h); //tegn rektanglen

  if (b > 150){ //hvis bredten er over 150, så skal den næste rektangel tegnes 50 pixels nede
    y += 50;
  }

  if (y > 500){ //hvis rektanglen er nået til bunden af canvas, så start forfra på 50
    y = 50;
  }


}


