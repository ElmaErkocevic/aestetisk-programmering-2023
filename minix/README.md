# MiniX oversigt
Her finder du beskrivelserne for alle miniX øvelser og feedback spørgsmål.

[🚪**CryptoPad oversigt over alles GitLab links og feedback organisering**](https://cryptpad.fr/sheet/#/2/sheet/edit/zGFZj7Xg-FcgjYXXMzD1HObJ/)

[[_TOC_]]

### MiniX[1]: RunMe og ReadMe (individuel)

💻 **Øvelse**: Aflevering søndag d.12 februar inden midnat<br>

[Øvelsen står beskrevet i grundbogen kap. 1](https://aesthetic-programming.net/pages/1-getting-started.html#minix-runme-and-readme-64484:~:text=think%20with%C2%A0it.-,MiniX,-%3A%20RunMe%20and%C2%A0ReadMe)

(Omkring 5000 anslag)


✍ **Feedback**: Aflevering onsdag d.15 februar inden undervisning <br> 

- Redegør for
    - Hvad ser du? Hvad handler programmet om?
    - Hvordan er det lavet? Hvilke (nye/interessante) syntakser bliver brugt?
    - Lykkeds det med at besvare øvelsen på en fyldesgørende, interessant eller unik måde?

- Reflektere over
    - Hvad er forskellen i at læse andres kode og at skrive din egen kode? Hvad kan du lære ved at læse andres miniX?
    - Hvilke ligeheder og forskelle er der mellem at skrive og at programmere?
    - Hvad er programmering og hvorfor skal vi kunne læse og skrive kode? - Relater til coding literacy og Aneette Vees tekst.

<br>


### MiniX[2]: Geometrisk emoji (individuel)

💻 **Øvelse**: Aflevering søndag d.19 februar inden midnat<br>

[Øvelsen står beskrevet i grundbogen kap. 2](https://aesthetic-programming.net/pages/2-variable-geometry.html#minix-geometric-emoji-62096:~:text=even%20transformed%C2%A0altogether.-,MiniX,-%3A%20Geometric%C2%A0emoji)


(Omkring 5000 anslag)


✍ **Feedback**: Aflevering onsdag d.22 februar inden undervisning <br> 

- Redegør for
    - Hvad ser du? Hvad handler programmet om?
    - Hvordan er det lavet? Hvilke (nye/interessante) syntakser bliver brugt?
    - Hvad får det dig til at tænke på eller føle?

- Reflektere over
    - Lykkeds det med at besvare øvelsen på en fyldesgørende, interessant eller unik måde?
    - Hvilket aspekt af programmet kan du bedst lide? Hvordan er udførelsen og tænkningen anderledes end din egen miniX?
    - Hvorvidt og hvordan adresserer værket æstetik eller politisk repræsentation via. det konceptuelle eller visuelle?


<br>

### MiniX[3]: Design en throbber (individuel)
❗**OBS: I har 2 uge til at lave denne miniX**

💻 **Øvelse**: Aflevering søndag d.5 marts inden midnat<br>

[Øvelsen står beskrevet i grundbogen kap. 3](https://aesthetic-programming.net/pages/3-infinite-loops.html#minix-designing-a-throbber-43062:~:text=beginnings%20and%C2%A0endings.-,MiniX,-%3A%20Designing%20a%C2%A0throbber)


(Omkring 5000 anslag)


✍ **Feedback**: Aflevering onsdag d.8 marts inden undervisning <br> 

- Redegør for
    - Hvad ser du? Hvad handler programmet om?
    - Hvordan er det lavet? Hvilke (nye/interessante) syntakser bliver brugt?
    - Hvad får det dig til at tænke på eller føle?

- Reflektere over
    - Lykkeds det med at besvare øvelsen på en fyldesgørende, interessant eller unik måde?
    - Hvilket aspekt kan du bedst lide? Hvordan er udførelsen og tænkningen anderledes end din egen miniX?
    - Hvilke koncepter eller citater fra litteraturen kan du relatere til eller bruge til at snakke om dette værk?

<br>

### MiniX[4]: Capture All (individuel)

💻 **Øvelse**: Aflevering søndag d.10 marts inden midnat<br>

[Øvelsen står beskrevet i grundbogen kap. 4](https://aesthetic-programming.net/pages/4-data-capture.html#voice-and-audio-data-12948:~:text=its%20other%C2%A0potentials.-,MiniX,-%3A%20Capture%C2%A0All)


(Omkring 5000 anslag)


✍ **Feedback**: Aflevering onsdag d.15 marts inden undervisning <br> 

- Redegør for
    - Hvad ser du? Hvad handler programmet om?
    - Hvad er det, der er blevet captured? Hvilken slags data er det?
    - Er der en explicit kobling til litteraturen? Hvis ja, hvilke konceptuelle koblinger og refleksioner er der til værket udover det tekniske?

- Reflektere over
    - Lykkeds det med at besvare øvelsen på en fyldesgørende, interessant eller unik måde?
    - Hvilket aspekt kan du bedst lide? Hvordan er udførelsen og tænkningen anderledes end din egen miniX?
    - Har du nogle forslag til denne miniX (kan være både ReadMe og RunMe?
    - Hvordan har dette værk ændret din forståelse af eller lært dig noget nyt om data capture?

<br>

### MiniX[5]: A Generative Program (individuel)

💻 **Øvelse**: Aflevering søndag d.19 marts inden midnat<br>

[Øvelsen står beskrevet i grundbogen kap. 5](https://aesthetic-programming.net/pages/5-auto-generator.html#minix-a-generative-program-23536:~:text=M.%20U.%C2%A0C.-,MiniX,-%3A%20A%20generative%C2%A0program)


(Omkring 3500-4000 anslag)


✍ **Feedback**: Aflevering onsdag d.22 marts inden undervisning <br> 

- Redegør for
    - Hvad ser du? Hvilke nye/interessante syntexer er blevet brugt?
    - Hvad er reglerne, som leder til generativitet og hvordan er reglerne implementeret?
    - Hvad er den konceptuelle kobling og refleksion af værket udover det tekniske? (fx. generativitet, regel-baseret systemer, (u)forudsigelighed, kontrol osv.)
    - Er der nogle koblinger til concepter fra litteraturen (både ÆP og SS)?

- Reflektere over
    - Kan du lide designet? Hvorfor/hvorfor ikke?
    - Hvordan har designprocessen været anderledes end din egen?
    - Hvordan hjælper værket dig til at forstå ideen med "auto-generator" (fx. niveauet af kontrol, autonomitet, kærlighed og omtanke via regler)?

<br>



### MiniX[6]: Games with objects (individuel)

💻 **Øvelse**: Aflevering søndag d.26 marts inden midnat<br>

[Øvelsen står beskrevet i grundbogen kap. 6](https://aesthetic-programming.net/pages/6-object-abstraction.html#minix-games-with-objects-87803:~:text=sense%20of%C2%A0purpose.-,MiniX,-%3A%20Games%20with%C2%A0objects)

(Omkring 3500-4000 anslag)

✍ **Feedback**: Aflevering onsdag d.29 marts inden undervisning <br> 

- Redegør for
    - Hvad er spillet og hvordan spiller man?
    - Hvad er objekternes egenskaber og adfærd?
    - Hvad udtrykker værket?
    - Hvad er den konceptuelle kobling og refleksion af værket udover det tekniske?

- Reflektere over
    - Hvilket aspekt kan du bedst lide? Hvorfor?
    - Hvordan interagere objekter med verdenen?
    - Hvordan er verdenssyn og ideologier indbygget i objekters egenskaber og adfærd?

<br>

### MiniX[7]: Genbesøg miniX (individuel)

💻 **Øvelse**: Aflevering søndag d.2 april inden midnat<br>

**Formål**:
- At indhente og videreudforske funktioner og syntakser fra tidligere undervisningsgange
- At genbesøge tidligere miniX og designe iterativt baseret på feedback og relfkesioner omkring det pågældende emne.
- At reflektere over den metodiske tilgang i Æstetisk Programmering

**Opgave (RunMe og ReadMe)**: 

Genbesøg og genarbejd en af dine tidligere miniX. Prøv at fokusere på metodologien i Æstetisk Programmering og det at tænke på teknologien som en middel til kritisk refleksion.
- Hvilken miniX har du valgt?
- Hvilke ændringer har du lavet og hvorfor?
- Hvad har du lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i din ReadMe/RunMe baseret på litteraturen vi har haft indtil nu?
- Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)? 

(Omkring 3500-4000 anslag)

✍ **Feedback**: INGEN FEEDBACK på denne miniX <br>

<br>


### MiniX[8]: Flowcharts (individuel + gruppe)
💻 **Øvelse**: Aflevering søndag d.16 april inden midnat<br>

Din egen repository skal både pege på din individuelle flowchart og gruppens.

[Øvelsen står beskrevet i grundbogen kap. 9](https://aesthetic-programming.net/pages/9-algorithmic-procedures.html#minix-flowcharts-41979:~:text=operations%20of%C2%A0programming.-,MiniX,-%3A%C2%A0Flowcharts)


(Omkring 3500-4000 anslag)

✍ **Feedback**: INGEN FEEDBACK på denne miniX

<br>


### MiniX[9]: E-lit (gruppe)
💻 **Øvelse**: Aflevering søndag d.23 april inden midnat<br>

[Øvelsen står beskrevet i grundbogen kap. 7](https://aesthetic-programming.net/pages/7-vocable-code.html#required-reading-94018:~:text=making%20binaries%C2%A0strange.-,MiniX,-%3A%C2%A0E%2Dlit)


(Omkring 3500-4000 anslag)

✍ **Feedback**: Aflevering onsdag d.26 april inden undervisning <br> 

- Redegør for
    - Hvad er programmet og hvordan virker det?
    - Er der nogle nye syntakser, som du ikke kendte til?
    - Hvad gør det til en e-lit (electronic literature)?
    - Er der en explicit kobling til litteraturen eller koncepter fra ÆP/SS?

- Reflektere over
    - Kan du lide designet? Hvorfor/hvorfor ikke?
    - Hvordan er brugen af gruppe strukture og JSON anderledes eller ens med din egen miniX?
    - Hvordan vil du analysere værket ift. perspektivet om kode og sprog, som åbner op for æstetisk refleksion?

<br>

### MiniX[10]: Machine unlearning (gruppe)
💻 **Øvelse**: Aflevering søndag d.30 maj inden midnat<br>

**Formål**:
- Selvstændigt at udforske og tinker med machine learning biblioteker og funktioner 
- At stille spørgsmål ved og forstå de eksisterende syntakser/logikker/programmeringsstrukture/funktioner, som relatere sig til machine learning

**Opgave (RunMe og ReadMe)**:

Denne gang behøver du ikke at designet et nyt koncept/værk/artefakt. Du skal derimod udvælge et eksempel kode fra ml5.js hjemmesiden (fx. ImageClassifier, PoseNet, StyleTransfer, CharRNN osv.), som du forsøger at forstå ved at køre den og ændre variablerne lokalt på din computer.

Hvis der opstår nye parameter, som du ikke kender eller forstår, så undersøg på nettet og find en måde at forstå de specifikke teknikaliteter ved machine learning (fx. temperatur i CharRNN, klassifikationer i ImgaeClassifier, RNN osv.).

Målet med denne miniX er at forstå, undersøge og diskuttere machine learning ved at engagere sig i de computationelle materialer, formulere forspørgsler til søgemaskinen, og at læse/køre kode.

Vis os et screenshot af din test og/eller din eksperimenterende sketch.

**ReadMe**:
- Hvilken eksempel kode har I valgt, og hvorfor?
- Har I ændret noget i koden for at forstå programmet? Hvilke?
- Hvilke linjer af kode har været særlig interessante for gruppen? Hvorfor?
- Hvordan vil du udforske/udnytte begrænsningerne af eksempel koden eller machine learning algoritmer?
- Var der syntakser/funktioner, som I ikke kendte til før? Hvilke? Og hvad lærte I?
- Hvordan kan I se en størrer relation mellem eksempel koden, som I har undersøgt, og brugen af machine learning ude i verdenen (fx. kreative AI, Voice Assistance, selvkørende biler, bots, ansigtsgenkendelse osv.)?
- Hvad kunne du tænke dig at vide mere om? Eller hvilke spørgsmål kan du ellers formulere ift. emnet?

(Omkring 3500-4000 anslag)

✍ **Feedback**: INGEN FEEDBACK på denne miniX, men hver gruppe skal præsentere miniX[10] om onsdagen <br> 

<br>


### MiniX[11]: Endelige projekt (gruppe) + draft
📝**Draft**: Aflevering søndag d.7 maj inden midnat

Lav et draft om jeres projekt på 3-4 sider (eksklusiv billeder, referencer og noter) i PDF format, og upload det til BrightSpace:

(Gå til BrightSpace > Course tools > Discussion > Endelige projekt draft aflevering > din gruppe > Start a new Thread)

PDF'en skal indeholder:
- En referenceliste
- Relevante sketches og billeder
- En titel på projektet og en revideret flowchart

✍ **Feedback**: Afgiv mundtlig feedback onsdag d.10 maj til vejledningsgangen <br> 

Forberede mundtlig feedback på 8 minutter om følgende emner:
- Brugen af litteratur og koncepter
- Kobling til temaerne i ÆP og kusets litteratur
- Tydeligheden af projektet (sproglig formidling)
- Hvad kan du bedst lide, og hvorfor?
- Hvilke elementer er mindre succesfulde? Hvilke forbedringsforslag har du?
- Øvrige kommentare


💻 **Øvelse**: Det endelige projekt skal afleveres onsdag d.17 maj<br>

[Øvelsen står beskrevet i grundbogen kap. 10](https://aesthetic-programming.net/pages/10-machine-unlearning.html#minix-final-project-24294:~:text=as%20a%C2%A0consequence%3F-,MiniX,-%3A%20final%C2%A0project)

Det er dette endelige projekt, som I vil blive spurgt ind til ved eksamen. MiniX[11] fungerer altså lidt som jeres synopsis.


<br>

<img src="https://media.giphy.com/media/12vJgj7zMN3jPy/giphy.gif">

