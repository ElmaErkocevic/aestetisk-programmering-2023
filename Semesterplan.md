# Æstetisk Programmering F23 Semesterplan
Her finder du semesterplanen for ÆP23. 

**OBS:** Jeres navne er noteret i semesterplanen, dér hvor I skal præsentere jeres miniX for klassen. Lav derfor en søgning på dit navn og orientere dig om hvornår du skal præsentere.

**Den supplerende læsning** er god at tage fat i, hvis man i sin minix føler sig uinspireret eller sidder fast med teorien. Så læs dem gerne hvis I har tid, hvis ikke, så skim dem i det mindste, så du ved hvad de handler om, og så du kan vende tilbage til dem hen mod eksamen.

*_Stjernemarkeret undervisningsgange betyder at Anders Visti er medunderviser_

[[_toc_]]

## Modul 1 | Uge 6 | 6 & 8 februar: Opstart - Code Literacy
Forbered dig på disse spørgsmål inden undervisningen:
-	Hvad har du af erfaring med programmering?
-	Hvilke programmeringssprog kender du? (fx HTML, CSS, Javascript, Processing, Java, C++ osv..)
-	Hvorfor skal vi lære at programmere i digital design uddannelsen?
-	Udover STEM tilgangen, hvilke andre måder kan vi ellers forestille os at lære programmering på?

**Litteratur**

Til mandag:
-	Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 13-24
-	Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93. (Uploadet til BrightSpace)
-	Video: Lauren McCarthy, Learning While making P5.JS, OPENVIS Conference (2015). https://www.youtube.com/watch?v=1k3X4DLDHdc&ab_channel=BocoupLLC 

Til onsdag:
-	Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
    - **OBS Code editor**: i stedet for Atom, som bogen henviser til, bruger vi [Visual Studio Code](https://code.visualstudio.com/)
-	Se de 5 videoer på Brightspace om web browsers, code editor, p5.js library, GitLab og Markdown.

**💻 MiniX[1]: RunMe og ReadMe (individuel)** [🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix1-runme-og-readme-individuel)
-	Aflevering søndag d.12 februar inden midnat
-	Feedback onsdag d.15 februar inden undervisning

## Modul 2 | Uge 7 | 13 & 15 februar: Sjov med geometri – Emojis

**Litteratur**
-	Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70
-	Video/tekst: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. **ELLER** Femke Snelting, Modifying the Universal, MedeaTV, 2016 [Video, 1 time 15 min].
-	Daniel Shiffman, Code! Programming with p5.js, The Coding Train [online] Kan findes på: https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [se videoerne 1.3, 1.4, 2.1, 2.2]

**Supplerende læsning**
-	Kort tekst: Femke Snelting, "Other Geometries", transmediale journal, Issue#3, 31 October 2019, https://transmediale.de/content/other-geometries
-	Crystal Abidin and Joel Gn, eds., "Histories and Cultures of Emoji Vernaculars," First Monday 23, no. 9, September (2018), https://firstmonday.org/ojs/index.php/fm/issue/view/607.
-	Christian Ulrik Andersen and Geoff Cox, eds., A Peer-Reviewed Journal About Machine Feeling 8, no. 1 (2019), https://aprja.net//issue/view/8133.
-	Derek Robinson, "Variables," in Matthew Fuller, ed., Software Studies: A Lexicon (Cambridge, MA: MIT Press, 2008).
-	Xin Xin, "1.2 2D Primitives - p5.js Tutorial," (2020) https://www.youtube.com/watch?v=hISICBkFa4Q 
-	Xin Xin, "1.3 Color and blendMode() - p5.js Tutorial," (2020) https://www.youtube.com/watch?v=fTEvHLLwSBE 

**💬 Præsentation af miniX[1] om onsdagen:**
- Maise Lesmann Alling
- Jonas Dahl Andersen
- Lisa Birungi Andersen
- Alexandra Domnica Bivol
- Amalie Doktor Skødt Simonsen

**💻 MiniX[2]: Geometrisk emoji (individuel)**[🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix2-geometrisk-emoji-individuel)
-	Aflevering søndag d.19 februar inden midnat
-	Feedback onsdag d.22 februar inden undervisning

## Modul 3 | Uge 8 | 20 & 22 februar: Infinity loops – Temporalitet 
Ingen shutup-and-code session om fredagen pga. gæsteforelæsning v/ Linda Hilfling Ritasdatter

**Litteratur**
-	Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
-	Daniel Shiffman, Courses 3.1, 3.3, 5.2, 9.1, 7.1, 7.2 Code! Programming with p5.js, (2018) https://thecodingtrain.com/beginners/p5js/index.html. (Indeholder praktisk brug af conditional statements, loops, functions, transformation, arrays)
-	Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://monoskop.org/log/?p=20190 

**Supplerende læsning**
-	Wolfgang Ernst, “‘... Else Loop Forever’: The Untimeliness of Media,” (2009), https://www.musikundmedien.hu-berlin.de/de/medienwissenschaft/medientheorien/ernst-in-english/pdfs/medzeit-urbin-eng-ready.pdf 
-	Daniel Shiffman, Code! Programming with p5.js, The Coding Train. (Indeholder praktisk brug af conditional statements, loops, functions and arrays – se videoerne 3.2, 3.4, 4.1, 4.2, 5.1, 5.3)
-	Xin Xin, "1.4 translate(), rotate(), push(), pop() - p5.js Tutorial," (2020) https://www.youtube.com/watch?v=maTfm84mLbo&list=PLT233rQkMw761t_nQ_6GkejNT1g3Ew4PU&index=4 
-	Wolfgang Ernst, Chronopoetics: The Temporal Being and Operativity of Technological Media (London: Rowman & Littlefield International, 2016), 63-95.
-	Winnie Soon, “Throbber: Executing Micro-temporal Streams,” Computational Culture 7, October 21 (2019), http://computationalculture.net/throbber-executing-micro-temporal-streams/ 
-	Derek Robinson, "Function," in Fuller, ed., Software Studies.

**💬 Præsentation af miniX[2] om onsdagen:**
- Camilla Boesen
- Nikolaj Boesen
- Christoffer Christensen
- Job Silas Nisgaard Ehrich
- Salomon Frederik K H Simonsen
- Abishana Tharumathas


**💻 MiniX[3]: Design en throbber (individuel)**[🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix3-design-en-throbber-individuel)

I har denne uge OG næste uge til at lave miniX[3]
-	Aflevering næste søndag d.5 marts inden midnat
-	Feedback næste næste onsdag d.8 marts inden undervisning

#### Friday Guestlecture 24 feb kl. 14.15-16 | Linda Hilfling Ritasdatter, Lessons in Crisis Computing
Find info om gæsteforelæsningen her: https://cc.au.dk/aktuelt/arrangementer/vis-arrangement/artikel/friday-lecture-linda-hilfling-ritasdatter-lessons-in-crisis-computing-1 

## Modul 4 | Uge 9 | 27 feb & 1 marts: Pause uge - Genbesøg miniX
Brug tid på at indhente læsning af litteratur, videoer, miniX, peer-feedback og hvad du ellers føler dig bagud med.

**Medbring til undervisningen** et stykke software kunst eller kritisk design, som indeholder programmering og som du synes er interessant.
Til undervisningsgangene stille vi skarpt på kursets formål og genbesøger de tidligere koncepter.

**Litteratur**

Genlæs Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 13-24 (vi vil snakke om det til undervisningen)

**💻 MiniX[3] – fortsat**

## Modul 5 | Uge 10 | 6 & 8 marts: Data capture
**Litteratur**

-	Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
-	“p5.js examples - Interactivity 1,” https://p5js.org/examples/hello-p5-interactivity-1.html 
-	“p5.js examples - Interactivity 2,” https://p5js.org/examples/hello-p5-interactivity-2.html 
-	“p5 DOM reference,” https://p5js.org/reference/#group-DOM 
-	Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021. https://policyreview.info/concepts/datafication 

**Supplerende læsning**

-	Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw 
-	Søren Pold, “Button,” in Fuller, ed., Software Studies.
-	Carolin Gerlitz and Anne Helmond, “The Like Economy: Social Buttons and the Data-Intensive Web,” New Media & Society 15, no. 8, December 1 (2013): 1348–65.
-	Christian Ulrik Andersen and Geoff Cox, eds., A Peer-Reviewed Journal About Datafied Research 4, no. 1 (2015), https://aprja.net//issue/view/8402 
-	Audun M. Øygard, “clmtrackr - Face tracking JavaScript library,” https://github.com/auduno/clmtrackr 
-	Daniel Shiffman, HTML / CSS/DOM - p5.js Tutorial (2017), https://www.youtube.com/playlist?list=PLRqwX-V7Uu6bI1SlcCRfLH79HZrFAtBvX 
-	Tiziana Terranova, “Red Stack Attack! Algorithms, Capital and the Automation of the Common,” EuroNomade (2014), http://www.euronomade.info/?p=2268.

**💬 Præsentation af miniX[3] om onsdagen:**
- Marius Storm Elmose
- Gustav Fridthiof Engbo
- Elma Erkocevic
- Jonas Hechmann Gejl
- Ann-Katrine Bidstrup Sørensen

**💻 MiniX[4]: Capture All (individuel)** [🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix4-capture-all-individuel)
-	Aflevering søndag d.10 marts inden midnat
-	Feedback onsdag d.15 marts inden undervisning

## Modul 6 | Uge 11 | 13 & 15 marts: Generativ kode *
Ingen shutup-and-code session om fredagen pga. gæsteforelæsning v/ Bernhard Siegert

**Litteratur**

-	Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
-	Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
-	Daniel Shiffman, “p5.js - 2D Arrays in Javascript,” Youtube, https://www.youtube.com/watch?v=OTNpiLUSiB4 

**Supplerende læsning**

-	Philip Galanter, “Generative Art Theory,” in Christiane Paul, ed., A Companion to Digital Art (Oxford: Blackwell, 2016), http://cmuems.com/2016/60212/resources/galanter_generative.pdf 
-	“How to Draw with Code | Casey Reas,” Youtube video, 6:07, posted by Creators, June 25 (2012), https://www.youtube.com/watch?v=_8DMEHxOLQE 
-	Daniel Shiffman, “p5.js Coding Challenge #14: Fractal Trees - Recursive,” https://www.youtube.com/watch?v=0jjeOYMjmDU 
-	Daniel Shiffman, “p5.js Coding Challenge #76: Recursion,” https://www.youtube.com/watch?v=jPsZwrV9ld0 
-	Daniel Shiffman, “noise() vs random() - Perlin Noise and p5.js Tutorial,” https://www.youtube.com/watch?v=YcdldZ1E9gU.

**💬 Præsentation af miniX[4] om onsdagen:**
- Joan Gunnarsdóttir Hansen
- Thea Lukassen Heskjær
- Mads Leth Hansen
- Isak Hegestand-Sverdlin
- Mathias Sund Sørensen

**💻 MiniX[5]: A Generative Program (individuel)** [🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix5-a-generative-program-individuel)

-	Aflevering søndag d.19 marts inden midnat
-	Feedback onsdag d.22 marts inden undervisning

#### Onsdag 15 marts kl.13-16 | DDINF Career Space

#### Friday Guestlecture 17 marts kl.14.15-16 | Bernhard Siegert, MediaNatures of Seawater – the Making of a Digitized Ocean
Find info om gæsteforelæsningen her: https://cc.au.dk/aktuelt/arrangementer/vis-arrangement/artikel/friday-lecture-bernhard-siegert-medianatures-of-seawater-the-making-of-a-digitized-ocean 

## Modul 7 | Uge 12 | 20 & 22 marts: Objekt orienteret programmering *
**Litteratur**

-	Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
-	Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (Uploadet til BrightSpace)
-	“p5.js examples - Objects,” https://p5js.org/examples/objects-objects.html 
-	“p5.js examples - Array of Objects,” https://p5js.org/examples/objects-array-of-objects.html 
-	Daniel Shiffman, “Code! Programming with p5.js,” The Coding Train (se videoerne 2.3, 6.1, 6.2, 6.3, 7.1, 7.2, 7.3), https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA 

**Supplerende læsning**
-	Cecile Crutzen and Erna Kotkamp, “Object Orientation,” in Fuller, ed., Software Studies, 200-207.
-	Roger Y. Lee, “Object-Oriented Concepts,” in Software Engineering: A Hands-On Approach (Springer, 2013), 17-24, 35-37.
-	Daniel Shiffman, “16.17 Inheritance in JavaScript - Topics of JavaScript/ES6,” https://www.youtube.com/watch?v=MfxBfRD0FVU&feature=youtu.be&fbclid=IwAR14JwOuRnCXYUIKV7DxML3ORwPIttOPPKhqTCKehbq4EcxbtdZDXJDr4b0 
-	Andrew P. Black, “Object-Oriented Programming: Some history, and challenges for the next fifty years” (2013), https://arxiv.org/abs/1303.0427.

**💬 Præsentation af miniX[5] om onsdagen:**
- Kathrine Morgenstjerne Hansen
- Martin Christoffersen Holm
- Kevin Klyssing Jensen
- Laura Kirstine Møller Jensen
- Sif Wilja Steenfeldt


**💻 MiniX[6]: Games with objects (individuel)** [🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix6-games-with-objects-individuel)

- Aflevering søndag d.26 marts inden midnat
- Feedback onsdag d.29 marts inden undervisning

#### Torsdag d. 23/3 kl.13-16 | DDLab workshop - Lasercut & plot p5js sketches
Mere info følger..

## Modul 8 | Uge 13 | 27 & 29 marts: Pause uge - Genbesøg miniX
Ingen nyt litteratur. Brug tid på at indhente læsning af litteratur, videoer, miniX, peer-feedback og hvad du ellers føler dig bagud med. 

Til undervisningen mandag skal vi snakke om eksamen og gruppedannelse og lave midtvejsevaluering. Onsdag afholder vi en coding-camp.

**💬 Præsentation af miniX[6] om onsdagen:**
- Signe Ursula Ulbrandt Jensen
- Rikke Osmann Jessen
- Carl Mathias Vestbo Johansen
- Jens Damgård Jørgensen
- Marta Tausen
- Kristin Trang Linh Vu

**💻 MiniX[7]: Genbesøg miniX (individuel)** [🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix7-genbes%C3%B8g-minix-individuel)
-	Aflevering søndag d.2 april inden midnat
-	INGEN FEEDBACK på denne miniX

### Onsdag d. 29/3 kl. 8-15 | ✨ Coding-camp – Lav din egen hjemmeside
Coding-camp er en hel workshopsdag, hvor vi er i mere uformelle rammer, hygger med nogle snacks, hører noget musik, og bare programmerer sammen. Vi skal tilbringe dagen på at kigge mere på HTML og CSS, og se på hvordan hjemmesider er opbygget og prøve at lave vores egen hjemmeside. Mere info følger..

## 🐣 | Uge 14 | PÅSKEFERIE
**PÅSKE – INGEN UNDERVISNING**

Påskeferie starter denne uge, så der er ingen undervisning, instruktortimer eller shutupandcode.

## Modul 9 | Uge 15 | 12 april: Algorithmic procedures 
**Mandag: 2. PÅSKEDAG – INGEN UNDERVISNING**

**Onsdag:** kl. 8-12

**Litteratur**
-	Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 211-226
-	Taina Bucher, “The Multiplicity of Algorithims,” If...Then: Algorithmic Power and Politics (Oxford: Oxford University Press, 2018), 19–40. (tilgængelig på AU library)
Supplerende læsning
-	Nathan Ensmenger, “The Multiple Meanings of a Flowchart,” Information & Culture: AJournal of History 51, no.3 (2016): 321-351, Project MUSE, doi:10.1353/lac.2016.0013.
-	Ed Finn, “What is an Algorithm,” in What Algorithms Want (Cambridge, MA: MIT Press, 2017), 15-56. Andrew Goffey, “Algorithm,” in Fuller, ed., Software Studies, 15-20.
-	Marcus du Sautoy, “The Secret Rules of Modern Living: Algorithms,” BBC Four (2015), https://www.bbc.co.uk/programmes/p030s6b3/clips.

**💬 Præsentation af miniX[7] om onsdagen:**
- Sultan Shamil Jevich Khurulbaev
- Signe Arndal Martens
- Sofie-Amalie Møller
- Emil Sommer Pilgaard Mortensen

**💻 MiniX[8]: Flowcharts (individual + gruppe)** [🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix8-flowcharts-individuel-gruppe)

-	Aflevering søndag d.16 april inden midnat
-	INGEN FEEDBACK på denne miniX

## Modul 10 | Uge 16 | 17 & 19 april: Vocable code *
**Litteratur**

-	Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186
-	Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38. (Uploadet til BrightSpace)
-	Allison Parrish, “Text and Type” (2019), https://creative-coding.decontextualize.com/text-and-type/ 
-	Daniel Shiffman, “10.2: What is JSON? Part I - p5.js Tutorial” (2017), https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r  
-	Daniel Shiffman, “10.2: What is JSON? Part II - p5.js Tutorial” (2017), https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r 

**Supplerende læsning**

-	Geoff Cox, Alex McLean, and Adrian Ward, “The Aesthetics of Generative Code,” Proceedings of Generative Art Conference, Milan (2001).
-	Liz W. Faber, The Computer’s Voice: From Star Trek to Siri (Minneapolis, MN: University of Minnesota Press, 2020).
-	Rita Raley, “Interferences: Net.Writing and the Practice of Codework,” Electronic Book Review (2002), http://electronicbookreview.com/essay/interferences-net-writing-and-the-practice-of-codework/ 
-	Margaret Rhee, “Reflecting on Robots, Love, and Poetry,” XRDS: Crossroads 24, no. 2, December (2017): 44–46, https://dl.acm.org/doi/pdf/10.1145/3155126?download=true 
-	Douglas Crockford, “The application/json Media Type for JavaScript Object Notation (JSON).” RFC 4627 (2006), https://www.ietf.org/rfc/rfc4627.txt.

**💬 Præsentation af miniX[8] (den individuelle) om onsdagen:**
- Nadezhda Georgieva Naneva
- Charlotte Øster Rasmussen
- Malthe Hjort Rasmussen
- Buster Siem
- Thea Maria Uhd

**💻 MiniX[9]: E-lit (gruppe)** [🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix9-e-lit-gruppe)
-	Aflevering søndag d.23 april inden midnat
-	Feedback onsdag d.26 april inden undervisning

#### Fredag d. 21/4 kl. 10-13 | DDLab Workshop - Phyiscal computing og p5js
Mere info følger…

## Modul 11 | Uge 17 | 24 & 26 april: Machine Learning *
**Litteratur**
-	Soon Winnie & Cox, Geoff, "Machine unlearning", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 227-252
-	Geoff Cox, “Ways of Machine Seeing,” Unthinking Photography (2016), https://unthinking.photography/articles/ways-of-machine-seeing 

**Supplerende læsning**
-	Kate Crawford and Vladan Joler, “Anatomy of an AI System: The Amazon Echo as an Anatomical Map of Human Labor, Data and Planetary Resources,” AI Institute (2018), https://anatomyof.ai/ 
-	Shakir Mohamed, Marie-Therese Png, William Isaac, “Decolonial AI: Decolonial Theory as Sociotechnical Foresight in Artificial Intelligence,” Philosophy & Technology, Springer, July 12 (2020), https://doi.org/10.1007/s13347-020-00405-8 
-	Adrian Mackenzie and Anna Munster, “Platform Seeing: Image Ensembles and Their Invisualities,” Theory, Culture & Society 26, no. 5 (2019): 3-22.
-	Daniel Shiffman, “Beginners Guide to Machine Learning in JavaScript,” The Coding Train, https://www.youtube.com/playlist?list=PLRqwX-V7Uu6YPSwT06y_AEYTqIwbeam3y

**💻 MiniX[10]: Machine unlearning (gruppe)** [🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix10-machine-unlearning-gruppe)
-	Aflevering søndag d.30 maj inden midnat
-	INGEN FEEDBACK på denne miniX, men hver gruppe skal præsentere miniX[10] om onsdagen

## Modul 12 | Uge 18 | 1 & 3 maj: Projektarbejde + draft pitch
**Mandag: INGEN UNDERVISNING – Arbejd på jeres endelige projekt!**

**Onsdag:**

Forberede et 5 minutters pitch af jeres endelige projekt:
-	Hvilket emne vil I gerne belyse? 
-	Hvilke litteraturer vil I kunne bruge?
-	Hvad handler jeres software om?
o	Hvis I har – gennemgå din idé via et flowchart
-	Hvilke udfordringer står I overfor nu?
Dagsorden:
-	Gruppepræsentation af miniX[10]: Machine unlearning
-	5 minutters pitch om det endelige projekt
-	Arbejdstid til projekt og projekt draft 

**💻 MiniX[11]: Endelige projekt (gruppe) + draft** [🡽](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/tree/main/minix#minix11-endelige-projekt-gruppe-draft)
-	Draft (3-4 sider) aflevering søndag d.7 maj inden midnat
-	Feedback (mundtlig) onsdag d.10 maj til vejledningsgangen
-	OBS: Det endelige projekt skal afleveres onsdag d.17 maj

## Modul 13 | Uge 19 | 8 & 10 maj: Vejledningsgang
**Mandag:**

Ingen undervisning, men forberede en detaljeret peer-feedback til den anden gruppes projekt i det samme vejledningstidspunkt, og arbejd videre på jeres endelige projekt.

**Onsdag:**

Vejledningsgang – Info om tidspunkter og grupper følger…

## Modul 14 | Uge 20 | 17 maj: Eksamensforberedelse og præsentationsrunde *
**Mandag:**

Ingen undervisning

**Onsdag: kl.8-13**

**OBS**: deadline for at aflevere det endelige projekt og præsentation!
Dagen bruges på fremlæggelser af det endelige projekt:
-	8 minutters præsentation med demonstration af software 
-	5 minutters Q&A for hver gruppe
Eksamenssnak og slutevaluering 

## Eksamen: Uge 25, 19/6 – 22/6

<img src=https://media.giphy.com/media/8dYmJ6Buo3lYY/giphy.gif>
